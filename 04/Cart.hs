module Cart where

type Title = String

type RUB = Double

type SimpleCart = [(Title, RUB)]

cartExample :: SimpleCart
cartExample = [("apples", 20.0), ("oranges", 30.0)]

cartTotal :: SimpleCart -> RUB
cartTotal [] = 0
cartTotal ((_, cost) : items) = cost + cartTotal items

cartTotal2 :: SimpleCart -> RUB
cartTotal2 = foldl f 0
  where
    f :: RUB -> (Title, RUB) -> RUB
    f acc (_, cost) = acc + cost

cartTotal3 :: SimpleCart -> RUB
cartTotal3 = sum . (map snd)

type Item = (Title, RUB)

type Amount = Double

type Cart = [(Amount, Item)]

cartExample2 :: Cart
cartExample2 = [(1, ("apples", 20.0)), (3.5, ("oranges", 30.0))]

amountCost :: (Amount, Item) -> RUB
amountCost (amount, (_, cost)) = amount*cost

cartTotal4 :: Cart -> RUB
cartTotal4 [] = 0
cartTotal4 (x : xs) = amountCost x + cartTotal4 xs

cartTotal5 :: Cart -> RUB
cartTotal5 = foldl f 0
  where
    f :: RUB -> (Amount, Item) -> RUB
    f acc item = acc + amountCost item

cartTotal6 :: Cart -> RUB
cartTotal6 = sum . map amountCost
























