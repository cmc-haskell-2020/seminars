{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}

module Exts where

data MyRecord = MyRecord { field1 :: Int
                         , field2 :: String
                         } deriving Show

-- ScopedTypeVariables
f :: MyRecord -> String
f (MyRecord (f1 :: Int) (f2 :: String)) = show f1 ++ f2

-- RecordWildCards
g :: MyRecord -> String
g rec@MyRecord{..} = show (field1 :: Int) ++ (field2 :: String) ++
                     show (rec :: MyRecord)

-- Without GeneralizedNewtypeDeriving: Can't make a derived instance of ‘Num RUB’
-- With GeneralizedNewtypeDeriving:
-- Exts> (RUB 100) + (RUB 12)
-- RUB 112.0
newtype RUB = RUB Double deriving (Show, Num)

-- TupleSections
tuples :: a -> b -> [c] -> [(a, c, b)]
tuples x y zs = map (x ,, y) zs
-- (x ,, y) = \z -> (x, z, y)

h :: (String, Int, String)
h = (,2,) "str1" "str2"
--  (\x y -> (x, 2, y))

-- LambdaCase
fromMaybeList :: a -> [Maybe a] -> [a]
fromMaybeList def = map
  (\case
    Just x -> x
    _      -> def
  )
