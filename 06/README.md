# Семинар 24.03.2020

# Классы типов

Несколько примеров полезных классов типов:

1. `Default` -- для указания значения по умолчанию.

    class Default a where
      def :: a


2. `IsString` -- для записи значений в виде строковых литералов:

    {-# LANGUAGE OverloadedStrings #-}
    class IsString a where
      fromString :: String -> a

3. `Listable` -- для типов, представимых в виде списка:

    class Listable a b where
      toList :: a -> [b]
      fromList :: [b] -> a

4. `Collection` -- общий интерфейс для коллекции:

    class Eq e => Collection c e where
      insert :: c -> e -> c
      member :: c -> e -> Bool

5. `Coercible` -- типы, допускающие преобрзование:

    class Coercible a b where
      coerce :: a -> b

# Виды типов

Haskell позволяет определять параметрические типы, такие как `Maybe a` или
`Either a b`. Сами по себе слова `Maybe` и `Either` не называют тип,
а являются конструкторами типов от одного и двух аргументов соответственно.
Невозможно объявить значение `m :: Maybe`, нужно передать конструктору
`Maybe` какой-то тип, например `mInt :: Maybe Int`. Можно рассматривать
`Maybe` как функцию над типами, которая принимает тип и возвращает тоже тип.
Аналогично и для `Either`, только этому конструктору нужно передать два
аргумента. Есть и "константные" конструкторы типов от нуля аргументов,
такие как `Int`, `Float`, `Char` и т.д. К ним также будут относиться
`Maybe Char` или `Either String Int`.

Чтобы отличать такие объекты, вводится "тип типов", который называется "вид"
(kind). На Haskell это записывается так:

```haskell
Int :: *
Maybe :: * -> *
Either :: * -> * -> *
```

Например, определим виды следующих типов:

```haskell
data A a = A a
data B a = B a (B a)
data D a = D
-- A, B, D :: * -> *

data I f a = I (f a)
-- I :: (* -> *) -> * -> *
data J f g a = J (f (g a))
-- J :: (* -> *) -> (* -> *) -> * -> *
data K a f b = K a
-- K :: * -> * -> * -> *
data M f a = MP a | MF (f (M f a))
-- M :: (* -> *) -> * -> *
data O t m a = O (t m a)
-- O :: O :: (* -> * -> *) -> * -> * -> *
```

# Домашнее задание

Определить вид типов:

```haskell
data C a b = C a (C b a) | CN
data E = E
data F a b = F (a -> b)
data G a b = G1 (a -> b) | G2 (b -> a)
data H a b = H ((b -> a) -> a)
data L f = L f (L f)
data N r f a = N ((a -> f r) -> f r)
```
