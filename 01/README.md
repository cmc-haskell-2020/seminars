# Семинар 18.02.2020

# Git

1. Интерактивный курс по работе с Git: [GitHowTo](https://githowto.com/).
2. Добавление имени ветки в приглашение ко вводу (prompt) в терминале: [git-prompt.sh](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh). Инструкция по установке в комментариях.
3. GitLab и SSH-ключи: https://gitlab.com/help/ssh/README

# Stack

1. Перед первой сборкой проекта нужно запустить команду `stack setup`.
2. Чтобы собрать проект: `stack build` или `stack build --file-watch`. Во втором случае
stack будет пересобирать проект автоматически при изменении исходников.
3. Для запуска исполняемого файла запустите команду `stack exec myproj`, где
`myproj` -- имя исполняемого файла (указывается в файле `package.yaml` в
разделе `executables`).

# Vim

1. Чтобы выйти из Vim, наберите `:q`.
2. Обучающий курс по работе с Vim запускается командой `vimtutor`.
3. Плагин для отображения дерева файлов: [NERDTree](https://github.com/scrooloose/nerdtree).

# Taskbot

На семинаре в качестве примера был реализован простой бот для
управления задачами.

[Скринкаст](https://drive.google.com/file/d/1a_lnh5s7q6K_fs0Gt0OSRPYwtGuYyCrz/view?usp=sharing).

Исходный код: [Main.hs](Main.hs) и [TaskBot.hs](TaskBot.hs).
