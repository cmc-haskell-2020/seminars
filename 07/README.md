# Семинар 07.04.2020

# Моноиды

Моноид -- множество, на котором задана бинарная ассоциативная операция и
нейтральный элемент. В Haskell моноиды описываются классом типов `Monoid`:

```haskell
class Monoid a where
  mempty  :: a
  mappend :: a -> a -> a
```

Инфиксный оператор `<>` -- более удобная запись `mappend`:
```haskell
(<>) = mappend
```

Для реализации методов класса для конкретнов типов должны выполняться следующие
ограничения:
  - Нейтральный элемент:
    ```haskell
    mempty <> x = x
    x <> mempty = x
    ```
  - Ассоциативность:
    ```haskell
    x <> (y <> z) = (x <> y) <> z
    ```

К сожалению, эти "теоретические" свойства моноида не провеяются компилятором,
за этим должен следить программист.

Рассмотрим примеры реализации:

```haskell
-- Внутреннее представление всегда должно быть отсортировано.
newtype SortedList a = SortedList [a]

instance Ord a => Monoid (SortedList a) where
  mempty = SortedList []
  mappend xs (SortedList []) = xs
  mappend (SortedList []) ys = ys
  mappend l1@(SortedList (x : xs)) l2@(SortedList (y : ys))
    | x <= y    = dummyCons x (mappend (SortedList xs) l2)
    | otherwise = dummyCons y (mappend l1 (SortedList ys))
    where
      dummyCons x (SortedList xs) = SortedList (x : xs)

-- Mean a хранит среднее и кол-во значений, из которых это среднее получено.
data Mean a = Mean Int a

instance Fractional a => Monoid (Mean a) where
  mempty = Mean 0 0
  mappend (Mean 0 _) (Mean 0 _) = mempty
  mappend (Mean n1 m1) (Mean n2 m2) =
    Mean (n1 + n2) ((n1'*m1 + n2'*m2)/(n1' + n2'))
    where
      n1' = fromIntegral n1
      n2' = fromIntegral n2

-- Обёртка для Maybe для получения последнего непустого значения
newtype Last a = Last { getLast :: Maybe a }

instance Monoid (Last a) where
  mempty = Last Nothing
  a <> Last Nothing = a
  _ <> b            = b
```
